# Puzzle Heroes

## Deux mots

On veut trouver l'algorithme le plus performant pour le problème qui suit. On vous passe deux mots contenant les caractères a à z en minuscule. On cherche à savoir si les deux mots passés contiennent exactement le même nombre de lettres et avec la même fréquence.

exemple: lorsque l'algorithme reçoit:

* `"allo"` et `"lola"`, retourne `true`
* `"allo"` et `"aaaallllloooo"`, retourne `false`
* `"allo"` et `"polo"`, retourne `false`

### Consignes

* L'algorithme doit s'exécuter en O(n) où n = nombre de lettres du mot 1 + nombre de lettres du mot 2
* L'espace mémoire utilisée doit être en O(1)

### Astuces
* Que se passe-t-il lorsqu'on passe un mot qui contient deux fois la même lettre à votre algo?
* Quels sont les temps d'exécution des opérations sur vos structures de données utilisées?
* Plusieurs boucles consécutives qui ont un temps d'exécution linéaire donne un algo ayant un temps d'exécution linéaire.

Rappel: La règle du maximum

```
Si f1(n) ∈ Θ(g1(n))
et f2(n) ∈ Θ(g2(n)),

alors

f1(n) + f2(n) ∈ Θ(max(g1(n), g2(n))).

Exemple: n + nlog(n) ∈ Θ(n log n)
```

### Réponse
Vous devez écrire votre pseudo-code dans ce README. Vous pouvez écrire votre démarche textuellement pour expliquer votre code et comment fait-il pour s'exécuter en O(n) et avoir O(1) en mémoire.

```
votre code ici!

```
alphabet = [a,b,c,d,e,f,g,h,i,j,h,i,j,k,l,m,n,o,p,q,r,s,t,u,w,x,y,z] // On met toutes les lettres qu'on veut traiter

checkMemeLongueurEtMemeFrequence(mot1, mot2)
    si mot1.longueur != mot2.longueur // O(n)
        return false
        
    presenceMot1 = {} // Ché pas comment faire un map en pseudo-code
    presenceMot2 = {} // Ché pas comment faire un map en pseudo-code

    pour tout lettre dans alphabet:
        presenceMot1[lettre] = 0
    pour tout lettre dans alphabet:
        presenceMot2[lettre] = 0 // L'espace utilisé par ces map sera toujours constant, 
                                  // car on a un int pour toutes les lettres de l'alphabet
                                  // et l'opération sera aussi constante comme on itère sur l'alphabet qui est constant
                       
    // Ces 2 opérations s'effectue en o(max(mot1.longueur, mot2.longueur)) = O(n)
    // et ne change pas l'utilisation memoire
    pour toute lettre dans mot1:
        presenceMot1[lettre]++
    pour toute lettre dans mot2:
        presenceMot2[lettre]++
    
    pour tout lettre dans alphabet:
        si presenceMot1[lettre] != presenceMot2[lettre]: // Si on ne compte pas le meme nombre de lettre dans les 2 mots, 
            return false                                 // alors ils n'ont pas les memes lettres aux memes frequences
    
    return true