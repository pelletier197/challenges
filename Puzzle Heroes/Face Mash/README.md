# Puzzle Heroes

## Face Mash

Pour chaque photo dans ce répertoire, vous devez trouver quels sont les personnages de la culture populaire qui ont été mélangés pour donner ces magnifiques portraits.
* Il y a toujours DEUX personnages par photo.
* Pour avoir le point vous pouvez inscrire jusqu'à trois noms de personnages pour essayer de tomber sur les deux bons. Vous avez donc droit à une erreur par photo.

Écrire les réponses dans ce README!

#### Réponses:

###### Photo 1
![](photo 1.png)

votre réponse:
Darill dans walking dead et ??

###### Photo 2
![](photo 2.png)

votre réponse:
Itachi
###### Photo 3
![](photo 3.png)

votre réponse:

###### Photo
![](photo 4.png)

votre réponse:
Mr. poopybutthole et ned flanders

###### Photo 5
![](photo 5.png)

votre réponse:
Khal drogo et dj keemstar
###### Photo 6
![](photo 6.png)

votre réponse:
Wilson le ballon et ??
###### Photo 7
![](photo 7.png)

votre réponse:
Ginette Reno et GLAdos

###### Photo 8
![](photo 8.png)

votre réponse:
FOX et ??
###### Photo 9
![](photo 9.png)

votre réponse: Clint eastwood

###### Photo 10
![](photo 10.png)

votre réponse: Nicolas cage et dr House
